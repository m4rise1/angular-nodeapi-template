# Node-express / Angular

Starting template for fullstack project with dockerized dev env.

Node.js Backend with Express and auth middlewares set up.
Angular Front (v17) initialized.

ESLINT and Prettier ready.

Package manager used : PNPM