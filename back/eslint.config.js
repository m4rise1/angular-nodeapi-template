// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
// import jestPlugin from 'eslint-plugin-jest';
import eslintConfigPrettier from "eslint-config-prettier";

export default tseslint.config(
  {
    // config with just ignores is the replacement for `.eslintignore`
    ignores: ['**/build/**', '**/dist/**',],
  },
  eslint.configs.recommended,
  ...tseslint.configs.strictTypeChecked,
  eslintConfigPrettier,
  {
    plugins: {
      '@typescript-eslint': tseslint.plugin,
    //   jest: jestPlugin,
    },
    languageOptions: {
      parser: tseslint.parser,
      parserOptions: {
        project: true,
        tsconfigRootDir: import.meta.dirname,
      },
    },
    rules: {
      '@typescript-eslint/no-unsafe-argument': 'error',
      '@typescript-eslint/no-unsafe-assignment': 'error',
      '@typescript-eslint/no-unsafe-call': 'error',
      '@typescript-eslint/no-unsafe-member-access': 'error',
      '@typescript-eslint/no-unsafe-return': 'error',
      "@typescript-eslint/prefer-nullish-coalescing": "error",
      "@typescript-eslint/prefer-string-starts-ends-with": "error",
      "@typescript-eslint/array-type": "warn",
      // Disable some useless strict rules
      "@typescript-eslint/no-misused-promises": "off",
    },
  },
  {
    // disable type-aware linting on JS files
    files: ['**/*.js'],
    ...tseslint.configs.disableTypeChecked,
  },
//   {
//     // enable jest rules on test files
//     files: ['test/**'],
//     ...jestPlugin.configs['flat/recommended'],
//   },
);