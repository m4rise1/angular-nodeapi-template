import { Response, response } from "express";

export default response.customSuccess = function (
	httpStatusCode: number,
	message: string,
	data: unknown = null,
): Response {
	return this.status(httpStatusCode).json({
		status: httpStatusCode,
		message,
		data,
	});
};
