import { NextFunction, Request, Response } from "express";

import { Role } from "../orm/entities/user/types";
import { CustomError } from "../utils/response/custom-error/CustomError";

export const checkRole = (roles: Role[], isSelfAllowed = false) => {
	return (req: Request, res: Response, next: NextFunction) => {
		const { id, role } = req.jwtPayload;
		const { id: requestId } = req.params;

		let errorSelfAllowed: string | null = null;
		if (isSelfAllowed) {
			if (id === parseInt(requestId)) {
				next();
				return;
			}
			errorSelfAllowed = "Self allowed action.";
		}

		if (!roles.includes(role)) {
			const errors = [
				"Unauthorized - Insufficient user rights",
				`Current role: ${role}. Required role: ${roles.toString()}`,
			];
			if (errorSelfAllowed) {
				errors.push(errorSelfAllowed);
			}
			const customError = new CustomError(
				401,
				"Unauthorized",
				"Unauthorized - Insufficient user rights",
				errors,
			);
			next(customError);
			return;
		}
		next();
	};
};
