import { NextFunction, Request, Response } from "express";

import dataSource from "../../../orm/config/ormconfig";
import { User } from "../../../orm/entities/user/User.entity";
import { CustomError } from "../../../utils/response/custom-error/CustomError";
import { ErrorValidation } from "../../../utils/response/custom-error/types";

export const validatorEdit = async (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	let { username } = req.body as Record<string, string>;
	const errorsValidation: ErrorValidation[] = [];
	const userRepository = dataSource.getRepository(User);

	username = !username ? "" : username;

	const user = await userRepository.findOne({
		where: {
			username,
		},
	});
	if (user) {
		errorsValidation.push({
			username: `Username '${username}' already exists`,
		});
	}

	if (errorsValidation.length !== 0) {
		const customError = new CustomError(
			400,
			"Validation",
			"Edit user validation error",
			null,
			null,
			errorsValidation,
		);
		next(customError);
		return;
	}
	next();
};
