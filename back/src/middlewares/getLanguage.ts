import { NextFunction, Request, Response } from "express";

import { Language } from "../orm/entities/user/types";

export const getLanguage = (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const acceptLanguageHeader = req.get("Accept-Language") as Language | null;
	if (!acceptLanguageHeader) {
		req.language = "en-US";
		next();
		return;
	}
	req.language = acceptLanguageHeader;
	next();
};
