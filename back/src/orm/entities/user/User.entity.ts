import bcrypt from "bcrypt";
import {
	Column,
	CreateDateColumn,
	Entity,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from "typeorm";

import { Language, Role } from "./types";

@Entity("users")
export class User {
	@PrimaryGeneratedColumn()
	id: number;

	@Column("varchar", {
		unique: true,
	})
	email: string;

	@Column("varchar")
	password: string;

	@Column("varchar", {
		nullable: true,
		unique: true,
	})
	username: string;

	@Column("varchar", {
		nullable: true,
	})
	name: string;

	@Column("varchar", {
		default: "STANDARD" as Role,
		length: 30,
	})
	role: string;

	@Column("varchar", {
		default: "fr-FR" as Language,
		length: 15,
	})
	language: string;

	@Column("date")
	@CreateDateColumn()
	created_at: Date;

	@Column("date")
	@UpdateDateColumn()
	updated_at: Date;

	setLanguage(language: Language) {
		this.language = language;
	}

	hashPassword() {
		this.password = bcrypt.hashSync(this.password, 8);
	}

	checkIfPasswordMatch(unencryptedPassword: string) {
		return bcrypt.compareSync(unencryptedPassword, this.password);
	}
}
