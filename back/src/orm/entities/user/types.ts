export type Role = "ADMINISTRATOR" | "STANDARD";
export type Language = "en-US" | "fr-FR";
