import { DataSource } from "typeorm";

import options from "../../orm/config/ormDataSourceOptions";

const dataSource = new DataSource(options);

export default dataSource;
