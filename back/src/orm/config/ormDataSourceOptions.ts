import { config } from "dotenv";
import { DataSourceOptions } from "typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategy";

config();
const options: DataSourceOptions = {
	type: "postgres",
	host: process.env.PG_HOST,
	port: Number(process.env.PG_PORT),
	username: process.env.POSTGRES_USER,
	password: process.env.POSTGRES_PASSWORD,
	database: process.env.POSTGRES_DB,
	entities: ["./../entities/**/*.entity.{js,ts}"],
	migrations: ["./../migrations/**/*.{js,ts}"],
	subscribers: ["./../subscriber/**/*.{js,ts}"],
	synchronize: false,
	migrationsRun: false,
	namingStrategy: new SnakeNamingStrategy(),
};

export default options;
