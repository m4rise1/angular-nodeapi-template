import dataSource from "../orm/config/ormconfig";

export const dbCreateConnection = async (): Promise<boolean> => {
	try {
		const dbConn = await dataSource.initialize();
		if (!dbConn.isInitialized) {
			throw new Error("Database connection is not initialized");
		}
		console.log(
			`Database connection success. Database: ${JSON.stringify(dataSource.options.database)}`,
		);
		return true;
	} catch (err: unknown) {
		console.log(
			`Error when attempting to connect to DB: ${err?.toString()}`,
		);
	}
	return false;
};
