import cors from "cors";
import { config } from "dotenv";
import express, { Express } from "express";
import helmet from "helmet";
import "reflect-metadata";

import { errorHandler } from "./middlewares/errorHandler";
import { dbCreateConnection } from "./orm/dbCreateConnection";
import routes from "./routes/routes";

class App {
	private app: Express;

	constructor() {
		this.app = express();
		this.globalMiddlewares();
		this.configureRoutes();
		this.globalErrorHandlerMiddleware();
	}

	globalMiddlewares(): void {
		this.app.use(cors());
		this.app.use(helmet());
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call
		this.app.use(express.json());
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call
		this.app.use(express.urlencoded({ extended: false }));
	}

	configureRoutes(): void {
		this.app.use("/", routes);
	}

	globalErrorHandlerMiddleware(): void {
		this.app.use(errorHandler);
	}

	run() {
		dbCreateConnection()
			.then((dataSrc) => {
				if (dataSrc) {
					const port = process.env.PORT || 5000;
					this.app.listen(port, () => {
						console.log(`Server running on port : ${port}`);
					});
				} else {
					throw new Error(
						"Error when attempting to connect to Database : closing connection...",
					);
				}
			})
			.catch((err) => {
				console.error(err);
			});
	}
}

config();
const server = new App();
server.run();
