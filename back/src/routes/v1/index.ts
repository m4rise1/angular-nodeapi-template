import { Router } from "express";

import { mailerService } from "../../services/mailer/mailer.service";
import auth from "./auth";
import users from "./users";

/**
 * Routes for API v1
 */
const router = Router();

router.use("/auth", auth);
router.use("/users", users);
router.get("/email", async (req, res) => {
	// send mail with defined transport object
	const test = await mailerService.send({
		from: '"Damien DUVAL 👻" <contact@damien-duval.fr>', // sender address
		to: "test@example.com", // list of receivers
		subject: "Hello ✔", // Subject line
		text: "Yolooooooo", // plain text body
		html: "<b>Yolo?</b>", // html body
	});
	return res.send(test);
});

export default router;
