import { Router } from "express";

import { destroy, edit, list, show } from "../../controllers/users";
import { checkJwt } from "../../middlewares/checkJwt";
import { checkRole } from "../../middlewares/checkRole";
import { validatorEdit } from "../../middlewares/validation/users";

const router = Router();

router.get("/", [checkJwt, checkRole(["ADMINISTRATOR"])], list);

router.get(
	"/:id([0-9]+)",
	[checkJwt, checkRole(["ADMINISTRATOR"], true)],
	show,
);

router.patch(
	"/:id([0-9]+)",
	[checkJwt, checkRole(["ADMINISTRATOR"], true), validatorEdit],
	edit,
);

router.delete(
	"/:id([0-9]+)",
	[checkJwt, checkRole(["ADMINISTRATOR"], true)],
	destroy,
);

export default router;
