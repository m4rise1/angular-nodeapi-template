import { Router } from "express";

/**
 * Fallback 404 route
 */
const router = Router();

router.get("*", (req, res) => {
	return res.status(404).json("404 Not Found :(");
});

export default router;
