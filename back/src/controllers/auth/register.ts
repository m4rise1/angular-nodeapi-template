import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const register = async (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const { email, password } = req.body as Record<string, string>;

	const userRepository: Repository<User> = dataSource.getRepository(User);
	try {
		const user = await userRepository.findOne({ where: { email } });

		if (user) {
			const customError = new CustomError(
				400,
				"General",
				"User already exists",
				[`Email '${user.email}' already exists`],
			);
			next(customError);
			return;
		}

		try {
			const newUser = new User();
			newUser.email = email;
			newUser.password = password;
			newUser.hashPassword();
			await userRepository.save(newUser);

			res.customSuccess(200, "User successfully created.");
		} catch (err) {
			const customError = new CustomError(
				400,
				"Raw",
				`User '${email}' can't be created`,
				null,
				err,
			);
			next(customError);
			return;
		}
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
