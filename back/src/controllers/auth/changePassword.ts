import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const changePassword = async (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const { password, passwordNew } = req.body as Record<string, string>;
	const { id, name } = req.jwtPayload;

	const userRepository: Repository<User> = dataSource.getRepository(User);
	try {
		const user = await userRepository.findOne({ where: { id } });

		if (!user) {
			const customError = new CustomError(404, "General", "Not Found", [
				`User ${name} not found.`,
			]);
			next(customError);
			return;
		}

		if (!user.checkIfPasswordMatch(password)) {
			const customError = new CustomError(400, "General", "Not Found", [
				"Incorrect password",
			]);
			next(customError);
			return;
		}

		user.password = passwordNew;
		user.hashPassword();
		await userRepository.save(user);

		res.customSuccess(200, "Password successfully changed.");
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
