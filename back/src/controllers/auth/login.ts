import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { Role } from "../../orm/entities/user/types";
import { JwtPayload } from "../../types/JwtPayload";
import { createJwtToken } from "../../utils/createJwtToken";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const login = async (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const { email, password } = req.body as Record<string, string>;

	const userRepository: Repository<User> = dataSource.getRepository(User);

	try {
		const user = await userRepository.findOne({ where: { email } });

		if (!user) {
			const customError = new CustomError(404, "General", "Not Found", [
				"Incorrect email or password",
			]);
			next(customError);
			return;
		}

		if (!user.checkIfPasswordMatch(password)) {
			const customError = new CustomError(404, "General", "Not Found", [
				"Incorrect email or password",
			]);
			next(customError);
			return;
		}

		const jwtPayload: JwtPayload = {
			id: user.id,
			name: user.name,
			email: user.email,
			role: user.role as Role,
			created_at: user.created_at,
		};

		try {
			const token = createJwtToken(jwtPayload);
			res.customSuccess(
				200,
				"Token successfully created.",
				`Bearer ${token}`,
			);
		} catch (err) {
			const customError = new CustomError(
				400,
				"Raw",
				"Token can't be created",
				null,
				err,
			);
			next(customError);
			return;
		}
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
