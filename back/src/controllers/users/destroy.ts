import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const destroy = async (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const id = Number(req.params.id);

	const userRepository: Repository<User> = dataSource.getRepository(User);
	try {
		const user = await userRepository.findOne({ where: { id } });

		if (!user) {
			const customError = new CustomError(404, "General", "Not Found", [
				`User with id:${id} doesn't exists.`,
			]);
			next(customError);
			return;
		}
		await userRepository.delete(id);

		res.customSuccess(200, "User successfully deleted.", {
			id: user.id,
			name: user.name,
			email: user.email,
		});
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
