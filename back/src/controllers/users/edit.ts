import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const edit = async (req: Request, res: Response, next: NextFunction) => {
	const id = Number(req.params.id);
	const { username, name } = req.body as Record<string, string>;

	const userRepository: Repository<User> = dataSource.getRepository(User);
	try {
		const user = await userRepository.findOne({ where: { id } });

		if (!user) {
			const customError = new CustomError(
				404,
				"General",
				`User with id:${id} not found.`,
				["User not found."],
			);
			next(customError);
			return;
		}

		user.username = username;
		user.name = name;

		try {
			await userRepository.save(user);
			res.customSuccess(200, "User successfully saved.");
		} catch (err) {
			const customError = new CustomError(
				409,
				"Raw",
				`User '${user.email}' can't be saved.`,
				null,
				err,
			);
			next(customError);
			return;
		}
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
