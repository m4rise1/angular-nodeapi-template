import { NextFunction, Request, Response } from "express";
import { Repository } from "typeorm";

import dataSource from "../../orm/config/ormconfig";
import { User } from "../../orm/entities/user/User.entity";
import { CustomError } from "../../utils/response/custom-error/CustomError";

export const show = async (req: Request, res: Response, next: NextFunction) => {
	const id = Number(req.params.id);

	const userRepository: Repository<User> = dataSource.getRepository(User);
	try {
		const user = await userRepository.findOne({
			select: {
				id: true,
				username: true,
				name: true,
				email: true,
				role: true,
				language: true,
				created_at: true,
				updated_at: true,
			},
			where: {
				id,
			},
		});

		if (!user) {
			const customError = new CustomError(
				404,
				"General",
				`User with id:${id} not found.`,
				["User not found."],
			);
			next(customError);
			return;
		}
		res.customSuccess(200, "User found", user);
	} catch (err) {
		const customError = new CustomError(400, "Raw", "Error", null, err);
		next(customError);
		return;
	}
};
