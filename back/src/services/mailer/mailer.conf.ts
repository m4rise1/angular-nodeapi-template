import SMTPTransport from "nodemailer/lib/smtp-transport";

const mailerConfig: SMTPTransport.Options = {
	host: "inbucket", // Container name
	port: 2500, // SMTP port
	secure: false, // true for 465, false for other ports
};
export default mailerConfig;
