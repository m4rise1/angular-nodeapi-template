import { Transporter, createTransport } from "nodemailer";
import Mail from "nodemailer/lib/mailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";

import mailerConfig from "./mailer.conf";

class MailerTransport {
	private readonly smtpTransport: Transporter<SMTPTransport.SentMessageInfo>;
	constructor(transport: SMTPTransport.Options) {
		this.smtpTransport = createTransport(transport);
	}

	public getSmtpTransport() {
		return this.smtpTransport;
	}
}

class MailerService {
	smtpTransport: Transporter<SMTPTransport.SentMessageInfo>;
	constructor(smtpTransport: Transporter<SMTPTransport.SentMessageInfo>) {
		this.smtpTransport = smtpTransport;
	}
	public async send(mailerOptions: Mail.Options) {
		const info = await this.smtpTransport.sendMail(mailerOptions);
		this.smtpTransport.close();
		return info.messageId;
	}
}
export const mailerService = new MailerService(
	new MailerTransport(mailerConfig).getSmtpTransport(),
);
