import { Language } from "../../orm/entities/user/types";
import { JwtPayload } from "../../types/JwtPayload";

declare global {
	namespace Express {
		export interface Request {
			jwtPayload: JwtPayload;
			language: Language;
		}
		export interface Response {
			customSuccess: (
				httpStatusCode: number,
				message: string,
				data?: unknown,
			) => Response;
		}
	}
}
